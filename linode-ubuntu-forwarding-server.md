## This repository is use to set a ubuntu forwarding server for shadowsocks-r in digitalocean
The script will forward all traffic (both income and outcome from 443 port) to 443 port in digitalocean (target) machine's 443 port but limited to TCP protocol. 
It will also disable ping and ssh and reboot your machine, be sure to give the script the right ip address!!

## First, let us replace the kernel with bbr kernel
```
apt-get update && wget --no-check-certificate -qO 'bbr-on-ubuntu-16.sh' 'https://bitbucket.org/xetrafhan/anti-gfw/raw/3ebcd6b971100bdb0967201e8da067df7f7e171d/install-bbr-on-ubuntu-16.sh' && chmod a+x bbr-on-ubuntu-16.sh && bash bbr-on-ubuntu-16.sh -f
```

## Second, let us set forwading rules, forward 443 to digital_ocean's 443

```
wget https://bitbucket.org/xetrafhan/release-anti-gfw-tcp-forwarding/raw/929eb3fc1aa39e5eb9e977aead177c8b80c17091/linode-ubuntu-forwarding-server-1.sh && \
bash linode-ubuntu-forwarding-server-1.sh
```

## Third, change reverse dns to your domain 

## Finally, use `iptables --table nat --list` to check iptable rules