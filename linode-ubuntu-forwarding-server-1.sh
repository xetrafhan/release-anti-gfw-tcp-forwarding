while [[ $CONTINUE != "y" && $CONTINUE != "n" ]]; do
	read -p "Please type in digital ocean's server ip (aka target_ip): " -e TARGETIP
	echo "Here is your input ip:"
	echo $TARGETIP
	echo ""
	read -p "Continue ? [y/n]: " -e CONTINUE
done

while [[ $CONTINUE1 != "y" && $CONTINUE1 != "n" ]]; do
	read -p "Please type in the port you want to forward (Remember limited to TCP): " -e FORWARDPORT
	echo "Here is your input forwarding port:"
	echo $FORWARDPORT
	echo ""
	read -p "Continue ? [y/n]: " -e CONTINUE1
done

echo 1 > /proc/sys/net/ipv4/ip_forward
LOCALIP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
iptables -t nat -A PREROUTING -p tcp --dport $FORWARDPORT -j DNAT --to-destination $TARGETIP:$FORWARDPORT
iptables -t nat -A POSTROUTING -p tcp -d $TARGETIP --dport $FORWARDPORT -j SNAT --to-source $LOCALIP

sudo systemctl enable rc-local.service

echo "" > /etc/rc.local

echo "
echo 1 > /proc/sys/net/ipv4/ip_forward
LOCALIP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
iptables -t nat -A PREROUTING -p tcp --dport $FORWARDPORT -j DNAT --to-destination $TARGETIP:$FORWARDPORT
iptables -t nat -A POSTROUTING -p tcp -d $TARGETIP --dport $FORWARDPORT -j SNAT --to-source $LOCALIP
echo 1 >/proc/sys/net/ipv4/icmp_echo_ignore_all
sudo iptables -A INPUT -p tcp --dport 22 -j DROP
exit 0
" >> /etc/rc.local
